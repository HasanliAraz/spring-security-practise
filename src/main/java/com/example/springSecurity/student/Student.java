package com.example.springSecurity.student;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class Student {
   private int studentId;
   private String name;
}
